﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using TestTask.DataAccess.Interfaces;

namespace TestTask.DataAccess.Entities
{
    public class User : IdentityUser<Guid>, IEntity
    {
    }
}
