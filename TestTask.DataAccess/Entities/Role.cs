﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using TestTask.DataAccess.Interfaces;

namespace TestTask.DataAccess.Entities
{
    public class Role : IdentityRole<Guid>, IEntity
    {
        public const string AdminRole = "Administrator";

        public const string Librarian = "Librarian";

        public const string Client = "Client";
    }
}
