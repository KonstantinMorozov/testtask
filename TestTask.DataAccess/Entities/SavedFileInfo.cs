﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTask.DataAccess.Interfaces;

namespace TestTask.DataAccess.Entities
{
    public class SavedFileInfo : Entity
    {
        public string Path { get; set; }

        public string InitialFileName { get; set; }

        public string FileName { get; set; }

        public string FileExtension { get; set; }

        public string ContentType { get; set; }
    }
}
