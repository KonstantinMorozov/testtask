﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTask.DataAccess.Enums;
using TestTask.DataAccess.Interfaces;

namespace TestTask.DataAccess.Entities
{
    public class Book : Entity
    {
        public string Title { get; set; }

        public string Author { get; set; }

        public string Genre { get; set; }
    
        public string Publisher { get; set; }

        public virtual SavedFileInfo ImageInfo { get; set; }

        public BookState State { get; set; }

        public virtual ICollection<BookLog> BookLogs { get; set; }
    }
}
