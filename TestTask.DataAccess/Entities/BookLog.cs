﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTask.DataAccess.Entities
{
    public class BookLog : Entity
    {
        public virtual User User { get; set; }

        public virtual Book Book { get; set; }        

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
