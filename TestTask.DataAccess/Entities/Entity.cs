﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTask.DataAccess.Interfaces;

namespace TestTask.DataAccess.Entities
{
    public class Entity : IEntity
    {
        public Guid Id { get; set; }
    }
}
