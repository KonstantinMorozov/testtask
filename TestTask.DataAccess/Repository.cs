﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestTask.DataAccess.Interfaces;

namespace TestTask.DataAccess
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    {
        private readonly TestTaskDbContext _context;

        public Repository(TestTaskDbContext context)
        {
            _context = context;
        }

        public void Create(TEntity entity)
        {
            _context.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            if (entity != null)
            {
                _context.Remove(entity);
                _context.SaveChanges();
            }     
        }

        public void Delete (IEnumerable<TEntity> entities)
        {
            _context.RemoveRange(entities);
            _context.SaveChanges();
        }

        public IQueryable<TEntity> GetItems()
        {
            return _context.Set<TEntity>();
        }

        public void Update(TEntity entity)
        {
            _context.SaveChanges();
        }

        public TEntity Get(Guid id)
        {
            return _context.Set<TEntity>().Find(id);
        }
    }
}
