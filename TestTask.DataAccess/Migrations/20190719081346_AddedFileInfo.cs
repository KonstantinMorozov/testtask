﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TestTask.DataAccess.Migrations
{
    public partial class AddedFileInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PicturePath",
                table: "Book");

            migrationBuilder.AddColumn<Guid>(
                name: "ImageInfoId",
                table: "Book",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SavedFileInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Path = table.Column<string>(nullable: true),
                    InitialFileName = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    FileExtension = table.Column<string>(nullable: true),
                    ContentType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedFileInfo", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Book_ImageInfoId",
                table: "Book",
                column: "ImageInfoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Book_SavedFileInfo_ImageInfoId",
                table: "Book",
                column: "ImageInfoId",
                principalTable: "SavedFileInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Book_SavedFileInfo_ImageInfoId",
                table: "Book");

            migrationBuilder.DropTable(
                name: "SavedFileInfo");

            migrationBuilder.DropIndex(
                name: "IX_Book_ImageInfoId",
                table: "Book");

            migrationBuilder.DropColumn(
                name: "ImageInfoId",
                table: "Book");

            migrationBuilder.AddColumn<string>(
                name: "PicturePath",
                table: "Book",
                nullable: true);
        }
    }
}
