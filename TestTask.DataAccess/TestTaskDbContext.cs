﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestTask.DataAccess.Entities;
using TestTask.DataAccess.EntityConfigs;

namespace TestTask.DataAccess
{
    public class TestTaskDbContext : IdentityDbContext<User, Role, Guid>
    {
        public TestTaskDbContext(DbContextOptions<TestTaskDbContext> options) : base(options)
        {
                
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new BookConfiguration());
            builder.ApplyConfiguration(new BookLogConfiguration());
            builder.ApplyConfiguration(new SavedImageConfiguration());

            base.OnModelCreating(builder);
        }
    }
}
