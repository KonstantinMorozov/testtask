﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTask.DataAccess.Enums
{
    public enum BookState
    {
        /// <summary>
        /// Книга свободна
        /// </summary>
        Free = 0,
        
        /// <summary>
        /// Книга забронирована
        /// </summary>
        Reserved = 1,

        /// <summary>
        /// Книга отдана
        /// </summary>
        Lent = 2
    }
}
