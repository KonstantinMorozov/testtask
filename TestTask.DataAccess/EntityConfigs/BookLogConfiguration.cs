﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestTask.DataAccess.Entities;

namespace TestTask.DataAccess.EntityConfigs
{
    public class BookLogConfiguration : IEntityTypeConfiguration<BookLog>
    {
        public void Configure(EntityTypeBuilder<BookLog> builder)
        {
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
        }
    }
}
