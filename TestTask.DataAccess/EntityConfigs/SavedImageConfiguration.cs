﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestTask.DataAccess.Entities;

namespace TestTask.DataAccess.EntityConfigs
{
    public class SavedImageConfiguration : IEntityTypeConfiguration<SavedFileInfo>
    {
        public void Configure(EntityTypeBuilder<SavedFileInfo> builder)
        {
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
        }
    }
}