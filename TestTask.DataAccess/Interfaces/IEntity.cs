﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTask.DataAccess.Interfaces
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
