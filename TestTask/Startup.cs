﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentScheduler;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TestTask.DataAccess;
using TestTask.DataAccess.Entities;
using TestTask.DataAccess.Interfaces;
using TestTask.Extensions;

namespace TestTask
{
    public class Startup
    {
        public Startup(IConfiguration config)
        {
            configuration = config;
        }

        public IConfiguration configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<TestTaskDbContext>(options =>
            {
                options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
            });
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddIdentity<User, Role>(options =>
            {
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireDigit = false;
            })
            .AddEntityFrameworkStores<TestTaskDbContext>();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => 
                {
                    options.LoginPath = new PathString("/Account/Login");
                });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddFileService();

            var serviceProvider = services.BuildServiceProvider();
            DI.Initialize(serviceProvider);
            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, TestTaskDbContext dbContext, 
            RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            dbContext.Database.Migrate();
            Seed(roleManager, userManager).Wait();

            JobManager.Initialize(new TasksRegistry());
            JobManager.Start();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private async Task Seed(RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            await roleManager.CreateAsync(new Role { Name = Role.AdminRole });
            await roleManager.CreateAsync(new Role { Name = Role.Librarian });
            await roleManager.CreateAsync(new Role { Name = Role.Client });

            var admin = new User { Email = "Admin@mail.ru", UserName = "Admin" };
            await userManager.CreateAsync(admin, "111111");
            await userManager.AddToRoleAsync(admin, Role.AdminRole);

            var librarian = new User { Email = "Librarian@mail.ru", UserName = "Librarian" };
            await userManager.CreateAsync(librarian, "111111");
            await userManager.AddToRoleAsync(librarian, Role.Librarian);
        }
    }
}
