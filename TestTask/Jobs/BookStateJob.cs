﻿using FluentScheduler;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.DataAccess.Entities;
using TestTask.DataAccess.Enums;
using TestTask.DataAccess.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace TestTask.Jobs
{
    public class BookStateJob : IJob
    {
        private readonly object _lock = new object();

        public void Execute()
        {
            lock (_lock)
            {
                using (DI.ServiceProvider.CreateScope())
                {
                    var bookLogRepository = DI.ServiceProvider.GetService<IRepository<BookLog>>();

                    var logs = bookLogRepository.GetItems()
                        .Where(x => x.Book.State == BookState.Reserved && x.EndDate == null)
                        .Include(x => x.Book)
                        .ToList();

                    var logsForEndDate = logs.Where(x => x.StartDate.AddDays(3) <= DateTime.Now);

                    foreach (var log in logsForEndDate)
                    {
                        log.EndDate = DateTime.Now;
                        log.Book.State = BookState.Free;

                        bookLogRepository.Update(log);
                    }
                }
            }
        }
    }
}
