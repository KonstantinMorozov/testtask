﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TestTask.DataAccess.Entities;
using TestTask.DataAccess.Enums;
using TestTask.DataAccess.Interfaces;
using TestTask.Extensions;
using TestTask.Models;
using TestTask.Services.Interfaces;

namespace TestTask.Controllers
{
    public class BooksController : Controller
    {
        private readonly IRepository<Book> _bookRepository;
        private readonly IRepository<BookLog> _bookLogRepository;
        private readonly UserManager<User> _userManager;
        private readonly IHostingEnvironment _environment;
        private readonly IFileService _fileService;
        private readonly IRepository<SavedFileInfo> _imageInfoRepository;

        public BooksController(IRepository<Book> bookRepository, IRepository<BookLog> bookLogRepository, UserManager<User> userManager,
                                IHostingEnvironment environment, IFileService fileService, IRepository<SavedFileInfo> imageInfoRepository)
        {
            _bookRepository = bookRepository;
            _bookLogRepository = bookLogRepository;
            _userManager = userManager;
            _environment = environment;
            _fileService = fileService;
            _imageInfoRepository = imageInfoRepository;
        }

        [Authorize]
        [HttpGet]
        public ActionResult BookList(string searchBy, string value)
        {
            var books = _bookRepository.GetItems()
                .Select(x => new BookModel
                {
                    Id = x.Id,
                    Title = x.Title,
                    Author = x.Author,
                    Genre = x.Genre,
                    Publisher = x.Publisher,
                    State = x.State,
                    SavedImageInfo = new SavedFileInfoModel
                    {
                        FileName = x.ImageInfo.FileName
                    },
                    ReservatorName = x.BookLogs
                        .OrderByDescending(y => y.StartDate)
                        .Select(y => y.User.UserName)
                        .FirstOrDefault(),
                })
                .Search(searchBy, value)
                .OrderBy(x => x.Title);

            return View(books.ToList());
        }

        [Authorize(Roles = Role.Librarian)]
        [HttpGet]
        public ActionResult AddBook()
        {
            return View();
        }

        [Authorize(Roles = Role.Librarian)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBook(BookModel model)
        {
            var path = Path.Combine(_environment.WebRootPath, "images");
            var imageModel = _fileService.SaveImageAsync(path, model.Picture).Result;

            _bookRepository.Create(new Book
            {
                Title = model.Title,
                Author = model.Author,
                Genre = model.Genre,
                Publisher = model.Publisher,
                ImageInfo = new SavedFileInfo
                {
                    Path = imageModel.Path,
                    InitialFileName = imageModel.InitialFileName,
                    FileName = imageModel.FileName,
                    FileExtension = imageModel.FileExtension,
                    ContentType = imageModel.ContentType
                }
            });

            return RedirectToAction(nameof(BookList));
        }

        [HttpGet]
        [Authorize(Roles = Role.Librarian)]
        public ActionResult Edit(Guid id)
        {
            var model = _bookRepository.GetItems()
                .Where(x => x.Id == id)
                .Select(x => new BookModel
                {
                    Id = id,
                    Author = x.Author,
                    Genre = x.Genre,
                    Title = x.Title,
                    Publisher = x.Publisher,
                    SavedImageInfo = new SavedFileInfoModel
                    {
                        Id = x.ImageInfo.Id,
                        FileName = x.ImageInfo.FileName
                    }
                })
                .FirstOrDefault();

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = Role.Librarian)]
        public ActionResult Edit(BookModel model)
        {
            var book = _bookRepository.GetItems().FirstOrDefault(x => x.Id == model.Id);
            var imageInfo = _bookRepository.GetItems()
                .Where(x => x.Id == model.Id)
                .Select(x => x.ImageInfo)
                .FirstOrDefault();

            book.Author = model.Author;
            book.Genre = model.Genre;
            book.Title = model.Title;
            book.Publisher = model.Publisher;

            if (model.Picture != null)
            {
                var path = Path.Combine(_environment.WebRootPath, "images");

                _fileService.Delete(model.SavedImageInfo.Path);
                _imageInfoRepository.Delete(imageInfo);

                var imageModel = _fileService.SaveImageAsync(path, model.Picture).Result;

                book.ImageInfo = new SavedFileInfo
                {
                    Path = imageModel.Path,
                    InitialFileName = imageModel.InitialFileName,
                    FileName = imageModel.FileName,
                    FileExtension = imageModel.FileExtension,
                    ContentType = imageModel.ContentType
                };
            }

            _bookRepository.Update(book);

            return RedirectToAction(nameof(BookList));
        }

        [HttpGet]
        [Authorize(Roles = Role.Librarian)]
        public ActionResult Delete(Guid id)
        {
            var book = _bookRepository.Get(id);

            var imageInfo = _bookRepository.GetItems()
                .Where(x => x.Id == id)
                .Select(x => x.ImageInfo)
                .FirstOrDefault();

            var bookLog = _bookLogRepository.GetItems()
                .Where(x => x.Book.Id == id);

            if (book.State == BookState.Free)
            {
                _bookLogRepository.Delete(bookLog);
                _bookRepository.Delete(book);
                _imageInfoRepository.Delete(imageInfo);
                _fileService.Delete(imageInfo.Path);
            }

            return RedirectToAction(nameof(BookList));
        }

        [Authorize(Roles = Role.Client)]
        public async Task<IActionResult> MakeReservation(Guid id)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var book = _bookRepository.Get(id);

            if (book.State == BookState.Free)
            {
                book.State = BookState.Reserved;

                _bookLogRepository.Create(new BookLog
                {
                    Book = book,
                    User = user,
                    StartDate = DateTime.Now
                });
            }

            return RedirectToAction(nameof(BookList));
        }

        [Authorize(Roles = Role.Client)]
        public async Task<IActionResult> UndoReservation(Guid id)
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var book = _bookRepository.Get(id);

            if (book.State == BookState.Reserved)
            {
                book.State = BookState.Free;

                var bookLog = _bookLogRepository.GetItems()
                    .FirstOrDefault(x => x.Book.Id == id);

                bookLog.EndDate = DateTime.Now;

                _bookRepository.Update(book);
            }

            return RedirectToAction(nameof(BookList));
        }

        [Authorize(Roles = Role.Librarian)]
        public IActionResult Lend(Guid id)
        {
            var book = _bookRepository.Get(id);

            if (book.State == BookState.Reserved)
            {
                book.State = BookState.Lent;
                _bookRepository.Update(book);
            }

            return RedirectToAction(nameof(BookList));
        }

        [Authorize(Roles = Role.Librarian)]
        public IActionResult TakeBack(Guid id)
        {
            var book = _bookRepository.Get(id);

            if (book.State == BookState.Lent)
            {
                book.State = BookState.Free;

                var bookLog = _bookLogRepository.GetItems()
                        .FirstOrDefault(x => x.Book.Id == id);

                bookLog.EndDate = DateTime.Now;

                _bookRepository.Update(book);
            }

            return RedirectToAction(nameof(BookList));
        }

        [Authorize(Roles = Role.Librarian)]
        public IActionResult DeleteImage(Guid id)
        {
            var imageInfo = _imageInfoRepository.Get(id);

            var book = _bookRepository.GetItems()
                .Where(x => x.ImageInfo.Id == id)
                .FirstOrDefault();

            _fileService.Delete(imageInfo.Path);
            _imageInfoRepository.Delete(book.ImageInfo);
            book.ImageInfo = new SavedFileInfo();

            _bookRepository.Update(book);

            return RedirectToAction(nameof(BookList));
        }
    }
}