﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TestTask.DataAccess.Entities;
using TestTask.DataAccess.Interfaces;
using TestTask.Models;

namespace TestTask.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<Role> _roleManager;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<Role> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }

        [HttpGet]
        public IActionResult Registration()
        {            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registration(RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.UserName,
                    Email = model.Email,
                };

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {                    
                    await _userManager.AddToRoleAsync(user, "Client");                    
                    await _signInManager.PasswordSignInAsync(user, model.Password, true, false);
                    return RedirectToAction("BookList", "Books");
                }
                else
                {
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
                }
            }
            
            return View(model);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.UserName);
                if (user != null)
                {
                    var result = await _signInManager.PasswordSignInAsync(user, model.Password, true, false);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("BookList", "Books");
                    }
                }

                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }

            return View(model);
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(Login));
        }

        [HttpGet]
        [Authorize(Roles = Role.AdminRole)]
        public IActionResult Administration()
        {
            var users = _userManager.Users;
            return View(users.ToList());
        }

        [HttpGet]
        [Authorize(Roles = Role.AdminRole)]
        public IActionResult InsertPassword(Guid id)
        {            
            return View(id);
        }

        [HttpPost]
        [Authorize(Roles = Role.AdminRole)]
        public async Task<IActionResult> InsertPassword(string stringId, string password)
        {
            var user = await _userManager.FindByIdAsync(stringId);
            await _userManager.RemovePasswordAsync(user);
            await _userManager.AddPasswordAsync(user, password);

            return RedirectToAction(nameof(Administration));
        }

        [HttpGet]
        [Authorize(Roles = Role.AdminRole)]
        public async Task<IActionResult> DeleteUser(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());

            var userIsAdmin = await _userManager.IsInRoleAsync(user, Role.AdminRole);

            if (!userIsAdmin)
            {
                try
                {
                    await _userManager.DeleteAsync(user);
                }
                catch
                {
                    return RedirectToAction(nameof(Administration));
                }
            }           

            return RedirectToAction(nameof(Administration));
        }
        
        [Authorize(Roles = Role.AdminRole)]
        public IActionResult AddUser()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = Role.AdminRole)]
        public async Task<IActionResult> AddUser(AddUserModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.UserName,
                    Email = model.Email
                };

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, model.Role);
                    return RedirectToAction(nameof(Administration));
                }
                else
                {
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
                }
            }

            return View(model);
        }
    }    
}
