﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Models
{
    public class SavedFileInfoModel
    {
        public Guid Id { get; set; }

        public string Path { get; set; }

        public string InitialFileName { get; set; }

        public string FileName { get; set; }

        public string FileExtension { get; set; }

        public string ContentType { get; set; }
    }
}
