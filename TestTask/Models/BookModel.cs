﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TestTask.DataAccess.Enums;

namespace TestTask.Models
{
    public class BookModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Название книги")]
        public string Title { get; set; }

        [Display(Name = "Автор")]
        public string Author { get; set; }

        [Display(Name = "Жанр")]
        public string Genre { get; set; }

        [Display(Name = "Издатель")]
        public string Publisher { get; set; }
        
        [Display(Name = "Статус")]
        public BookState State { get; set; }

        public IFormFile Picture { get; set; }

        public SavedFileInfoModel SavedImageInfo { get; set; }

        public string ReservatorName { get; set; }
    }
}
