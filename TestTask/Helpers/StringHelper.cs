﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Helpers
{
    public static class StringHelper
    {
        public static bool IsNullOrEmpty(params string[] strings)
        {
            return strings.Any(x => string.IsNullOrEmpty(x));
        }
    }
}
