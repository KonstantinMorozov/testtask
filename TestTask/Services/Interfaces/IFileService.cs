﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Models;

namespace TestTask.Services.Interfaces
{
    public interface IFileService
    {
        Task<SavedFileInfoModel> SaveImageAsync(string directoryPath, IFormFile file);

        void Delete(string fullPath);
    }
}
