﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using TestTask.Models;
using TestTask.Services.Interfaces;

namespace TestTask.Services
{
    public class FileService : IFileService
    {
        public async Task<SavedFileInfoModel> SaveImageAsync(string directoryPath, IFormFile file)
        {
            if (string.IsNullOrEmpty(directoryPath) || file == null)
            {
                return new SavedFileInfoModel();
            }

            var extension = Path.GetExtension(file.FileName);
            var initialFileName = file.FileName; 
            var fileName = $"{Guid.NewGuid()}{extension}";
            var fullPath = Path.Combine(directoryPath, fileName);

            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return new SavedFileInfoModel
            {
                Path = fullPath,
                InitialFileName = initialFileName,
                FileName = fileName,
                FileExtension = extension,
                ContentType = file.ContentType
            };
        }

        public void Delete(string fullPath)
        {
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }

            return;
        }
    }
}
