﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using TestTask.Helpers;

namespace TestTask.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> Search<T>(this IQueryable<T> query, string searchBy, string value)
        {
            if (StringHelper.IsNullOrEmpty(searchBy, value))
            {
                return query;
            }

            var parameter = Expression.Parameter(typeof(T), "x");
            var property = Expression.Property(parameter, searchBy);

            if (property.Type == typeof(string))
            {
                var constant = Expression.Constant(value.ToUpper(), typeof(string));
                var toUpperMethod = property.Type.GetMethod(nameof(string.ToUpper), new Type[0]);
                var toUpperExpression = Expression.Call(property, toUpperMethod);
                var containsMethod = property.Type.GetMethod(nameof(string.Contains), new[] { typeof(string) });
                var containsExpression = Expression.Call(toUpperExpression, containsMethod, constant);
                var lambda = Expression.Lambda<Func<T, bool>>(containsExpression, parameter);

                return query.Where(lambda);
            }

            return query;
        }
    }
}
