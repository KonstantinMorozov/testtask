﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Services.Interfaces;
using TestTask.Services;

namespace TestTask.Extensions
{
    public static class UseFileServiceExtension
    {
        public static void AddFileService(this IServiceCollection services)
        {
            services.AddTransient<IFileService, FileService>();
        }
    }
}
